const express= require('express')
const db= require('../Utils/utils')
const router= express.Router()

router.get('/getall', (request, response)=>{
    const {movie_title, movie_release_date, movie_time, director_name}= request.body

    const connection= db.openConnection()

    const statement=`
    select * from movie
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(db.createResult(error, result))
    })

})

router.post('/add', (request, response)=>{
    const {movie_title, movie_release_date, movie_time, director_name}= request.body

    const connection= db.openConnection()

    const statement=`
    insert into movie
    (movie_title, movie_release_date, movie_time, director_name)
    values
    ('${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(db.createResult(error, result))
    })

})

router.put('/edit/:movie_title', (request, response)=>{
    
    const {movie_title}= request.params
    const {movie_release_date, movie_time}= request.body

    const connection= db.openConnection()

    const statement=`
    update movie set
    (movie_release_date, movie_time)
    where
    movie_title='${movie_title}'
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(db.createResult(error, result))
    })

})

router.delete('/delete/:movie_title', (request, response)=>{
    const {movie_title}= request.params

    const connection= db.openConnection()

    const statement=`
    delete from movie where
    movie_title='${movie_title}'
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(db.createResult(error, result))
    })
})

module.exports= router