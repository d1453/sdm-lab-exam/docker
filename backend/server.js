const express=require('express')
const cors= require('cors')
const routeMovie= require('./Routes/movie')

const app=express()

app.use(cors('*'))
app.use(express.json())
app.use('/movie', routeMovie)

app.listen(4040, '0.0.0.0', ()=>{
    console.log('server started on port 4040')    
})